package utcn.consumer;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import org.springframework.stereotype.Component;
import utcn.web.entity.DVD;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

@Component
public class ReceiverWriteToText implements MessageListener {

    public void onMessage(Message message) {
        String dvdString = "";
        if (message != null) {
            dvdString = message.toString().substring(message.toString().indexOf("'"), message.toString().lastIndexOf("'"));
            DVD dvd = DVD.fromString(dvdString);
            try {
                System.out.println("Writing to file...");
                OutputStream fileOutputStream = new FileOutputStream(new File("/" + dvd.getTitle()));
                fileOutputStream.write(("There is a new DVD:\n" +
                        "Title: " + dvd.getTitle() + ", Year: " + dvd.getYear() + ", Price: " + dvd.getPrice() + ".\n" +
                        "Go buy it!").getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
