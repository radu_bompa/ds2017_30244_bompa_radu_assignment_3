package utcn.web.controller;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import utcn.web.entity.DVD;

import java.io.IOException;

/**
 * Created by Radu Bompa
 * 12/5/2017
 */

@Controller
@RequestMapping("/")
public class DVDController {
	@Autowired
	private AmqpTemplate messageQueue;

	@RequestMapping(method = RequestMethod.GET)
	public String createDVDPage(ModelMap model) {
		return "createDVD";
	}

	@RequestMapping(method = RequestMethod.POST, path = "/addDVD")
	@ResponseBody
	public String addDVD(@RequestParam(value = "title", required = true) String title,
	                     @RequestParam(value = "year", required = true) Integer year,
	                     @RequestParam(value = "price", required = true) Double price) throws InterruptedException, IOException {
		DVD dvd = new DVD(title, year, price);
//		create message
		messageQueue.convertAndSend("dvd-mail", dvd);
		messageQueue.convertAndSend("dvd-text", dvd);

		return "DVD added!";
	}
}
