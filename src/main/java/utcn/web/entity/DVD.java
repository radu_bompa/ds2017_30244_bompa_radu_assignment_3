package utcn.web.entity;

import java.io.Serializable;

/**
 * Created by Radu Bompa
 * 11/28/2017
 */
public class DVD implements Serializable {
	private static long serialVersionUID = 4411114141L;

	private String title;
	private Integer year;
	private Double price;

	public DVD() {
	}

	public DVD(String title, Integer year, Double price) {
		this.title = title;
		this.year = year;
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	@Override
	public String toString() {
		return "DVD{" +
				"title='" + title + '\'' +
				", year=" + year +
				", price=" + price +
				'}';
	}

	public static DVD fromString(String dvdToString) {
		DVD dvd = null;
		if (dvdToString != null) {
			String title = dvdToString.substring(dvdToString.indexOf("=") + 1, dvdToString.indexOf(","));
			Integer year = Integer.parseInt(dvdToString.substring(dvdToString.indexOf("=", dvdToString.indexOf("=") + 1) + 1, dvdToString.lastIndexOf(",")));
			Double price = Double.parseDouble(dvdToString.substring(dvdToString.lastIndexOf("=") + 1, dvdToString.indexOf("}")));
			dvd = new DVD(title, year, price);
		}
		return dvd;
	}
}
