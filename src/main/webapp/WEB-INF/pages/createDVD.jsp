<html>
<body>
	<h2>Add new DVD</h2>
	<form action="${pageContext.request.contextPath}/addDVD" method="post">
		<div>
			<label for="title">Title:</label><input type="text" id="title" name="title"/>
		</div>
		<div>
			<label for="year">Year:</label><input type="text" id="year" name="year"/>
		</div>
		<div>
			<label for="price">Price:</label><input type="text" id="price" name="price"/>
		</div>
		<input type="submit" value="Add DVD">
	</form>
</body>
</html>